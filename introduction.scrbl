#lang scribble-acmart-example

@title{Introduction}

Scribble creates a connection between a stand-alone document and the artifact
it describes@~cite[fbf-icfp-2009].
