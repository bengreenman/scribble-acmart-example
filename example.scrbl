#lang scribble-acmart-example @sigplan @anonymous @review @10pt

@; ---

@(define neu (affiliation #:institution "Northeastern University"))
@(define anon (email "anon@anon.net"))

@title{Writing a paper with Scribble}
@author[#:affiliation neu #:email anon]{Ben Greenman}

@; optional: set the author names in the page headers
@elem[#:style "Sshortauthors"]{B. Greenman}

@; ---

@include-abstract{abstract.scrbl}
@include-section{introduction.scrbl}
@include-section{inline-latex.scrbl}
@include-section{standard-fish.scrbl}

@; ---

@generate-bibliography[#:sec-title "References"]
