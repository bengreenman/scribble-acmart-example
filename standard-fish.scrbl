#lang scribble-acmart-example
@require[pict]

@title{Picture of a Fish}

@Figure-ref{fig:fish} contains a picture of a blue standard fish.
Anything allowed in a normal Scribble document is allowed in a figure.

@figure-here[
  "fig:fish"  @; figure tag, see `figure-ref`
  @elem{A Standard Fish}  @; figure caption, appears below the content
  @elem{fish = @(standard-fish 90 40)}]  @; content
