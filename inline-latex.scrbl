#lang scribble-acmart-example

@title{Some Inline @elem[#:style "LaTeX"]}

The built version of this section contains the character sequence @tt|{\input{lambda.tex}}|
 and that sequence renders to the rendered content of the file @tt{lambda.tex}:

@elem[#:style "input"]{lambda.tex}
