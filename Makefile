DOC=example

all:
	raco make ${DOC}.scrbl
	raco scribble ++style style.tex ++extra lambda.tex --pdf ${DOC}.scrbl

clean:
	rm -r compiled
	rm ${DOC}.pdf
