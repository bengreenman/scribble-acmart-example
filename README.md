scribble-acmart-example
===

A very simple document using `scribble/acmart` and a custom `#lang`


Install / Usage
---

In this directory:

```
raco pkg install
make
```

Then open `example.pdf`


Uninstall
---

```
raco pkg remove scribble-acmart-example
```
